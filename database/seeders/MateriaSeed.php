<?php

namespace Database\Seeders;

use App\Models\materia;
use Illuminate\Database\Seeder;

class MateriaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $materia = new materia();
        $materia->nombre_materia="fundamentos de programacion";
        $materia->save();

        $materia = new materia();
        $materia->nombre_materia="Topicos Avanzados";
        $materia->save();

        $materia = new materia();
        $materia->nombre_materia="Bases de datos";
        $materia->save();

        $materia = new materia();
        $materia->nombre_materia="Arquitectura de software";
        $materia->save();

        $materia = new materia();
        $materia->nombre_materia="Metodologias agiles";
        $materia->save();

        $materia = new materia();
        $materia->nombre_materia="Fundamentos de investigacion";
        $materia->save();

        $materia = new materia();
        $materia->nombre_materia="Inteligencia Artificial";
        $materia->save();



    }
}
