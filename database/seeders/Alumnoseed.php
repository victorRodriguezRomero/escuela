<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\alumno;

class Alumnoseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $alumno = new alumno();
        $alumno->nombre = "victor manuel";
        $alumno->apellido_paterno = "Rodriguez";
        $alumno->apellido_materno = "Romero";
        $alumno->email = "victormanrro@hotmail.com";
        $alumno->telefono = "6622226044";
        $alumno->save();

        $alumno = new alumno();
        $alumno->nombre = "Jose Manuel";
        $alumno->apellido_paterno = "Matrecitos";
        $alumno->apellido_materno = "Almaraz";
        $alumno->email = "JoseMatrecitos_genio@hotmail.com";
        $alumno->telefono = "6621456790";
        $alumno->save();

        $alumno = new alumno();
        $alumno->nombre = "Heriberto";
        $alumno->apellido_paterno = "Valencia";
        $alumno->apellido_materno = "Gonzalez";
        $alumno->email = "Heriberto85_gonzalez@hotmail.com";
        $alumno->telefono = "6621670987";
        $alumno->save();

        $alumno = new alumno();
        $alumno->nombre = "uriel";
        $alumno->apellido_paterno = "Rodriguez";
        $alumno->apellido_materno = "Romero";
        $alumno->email = "urielroguez@hotmail.com";
        $alumno->telefono = "6622226034";
        $alumno->save();

        $alumno = new alumno();
        $alumno->nombre = "lazaro";
        $alumno->apellido_paterno = "Rodriguez";
        $alumno->apellido_materno = "Romero";
        $alumno->email = "lazaro86@hotmail.com";
        $alumno->telefono = "6621785467";
        $alumno->save();

        $alumno = new alumno();
        $alumno->nombre = "flavio";
        $alumno->apellido_paterno = "Rodriguez";
        $alumno->apellido_materno = "Romero";
        $alumno->email = "flavio84@hotmail.com";
        $alumno->telefono = "6622347890";
        $alumno->save();
    }
}
