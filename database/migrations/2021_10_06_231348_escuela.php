<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Escuela extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 25);
            $table->string("apellido_paterno", 12);
            $table->string("apellido_materno", 12);
            $table->string("telefono", 10);
            $table->string('email', 50)->unique();
            $table->timestamps();
        });

        Schema::create('materias', function (Blueprint $table) {
            $table->id();
            $table->string("nombre_materia", 50);
            $table->timestamps();
        });

        Schema::create('clases', function (Blueprint $table) {
            $table->id();
            $table->char("grupo", 2);
            $table->string("turno", 10);
            // $table->integer('id_alumno')->unsigned();
            $table->unsignedBigInteger('id_alumno');
            $table->foreign('id_alumno')->references('id')->on('alumnos');
            $table->unsignedBigInteger('id_materia');
            $table->foreign('id_materia')->references('id')->on('materias');
            $table->timestamps();
        });

        Schema::create('parciales', function (Blueprint $table) {
            $table->id();
            $table->char('numero_parcial', 1);
            $table->decimal("calificacion");
            $table->unsignedBigInteger("id_clase");
            $table->foreign("id_clase")->references("id")->on("clases");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
