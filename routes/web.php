<?php

use App\Http\Controllers\AlumnController;
use App\Http\Controllers\CLasesController;
use App\Http\Controllers\PartialController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// --rutas de alumnos--
Route::get('Alumno', [AlumnController::class, 'index'])->name('alumno.index');
Route::get("Alumno/create", [AlumnController::class, 'create'])->name('alumno.create');
Route::get("Alumno/edit/{id}", [AlumnController::class, 'edit'])->name('alumno.edit');
Route::post("Alumno/register", [AlumnController::class, 'add'])->name('alumno.register');
Route::put("Alumno/update/{id}", [AlumnController::class, 'update'])->name('alumno.update');
Route::get("Alumno/destroy/{id}", [AlumnController::class, 'destroy'])->name('alumno.destroy');

// --rutas de clases--
Route::get("Clase/create/{id}", [CLasesController::class, 'create'])->name('clase.create');
Route::get("Clase/get", [CLasesController::class, 'getclases'])->name('clase.index');
Route::post("Clase/stoke", [CLasesController::class, 'stoke'])->name('clase.stoke');

// --rutas de parciales--
Route::get("Partial/create/{id}", [PartialController::class, 'create'])->name('parcial.create');




