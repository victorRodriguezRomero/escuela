@extends('Layout.Principal')
@section('title', 'Alumno' )

@section('content')
<div class="container">

    <h1>Actualizacion de alumno</h1>
    <br>

    <form action="{{route('alumno.update', $alumno)}}" method="POST">
        @csrf
        @method('put');
        <label for="nombre">Nombre del alumno</label>
        <input type="text" name="nombre" value="{{$alumno->nombre}}">
        <br>
        @error('nombre')
        <small> {{$message}}</small>

        @enderror
        <br>
        <label for="apellido_paterno">Apellido Paterno</label>
        <input type="text" name="apellido_paterno" value="{{$alumno->apellido_paterno}}">
        <br>
        @error('apellido_paterno')
        <small> {{$message}}</small>

        @enderror
        <br>
        <label for="apellido_materno">Apellido materno</label>
        <input type="text" name="apellido_materno" value="{{$alumno->apellido_materno}}">
        <br>
        @error('apellido_materno')
        <small> {{$message}}</small>

        @enderror
        <br>

        <div class="form-group mt-4">
            <label for="email">Correo electronico</label>
            <input type="email" name="email" value="{{$alumno->email}}">
            <br>
            @error('email')
            <small> {{$message}}</small>

            @enderror
            <br>
            <label for="telefomo">Ingrese su telefono</label>
            <input type="tel" name="telefono" value="{{$alumno->telefono}}">
            <br>
            @error('telefono')
            <small> {{$message}}</small>

            @enderror
            <br>
            <input type="submit" class="btn btn-success" value="Actualizar alumno">
    </form>
</div>

@endsection