@extends('Layout.Principal')
@section('title', 'Alumno' )

@section('content')
<div class="container">

    <h1>Alumnos registrados</h1>
    <a class="btn btn-success" href="{{route('alumno.create')}}">Registrar alumno</a>
    <br>
    <br>
    <div class="row">
        <table class="table table-stripper table-success" id="table-alumn">
            <thead>
                <tr>
                    <td>Nombre del Alumno</td>
                    <td>apellido paterno</td>
                    <td>apellido materno</td>
                    <td>correo</td>
                    <td>telefono</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($alumnos as $alumno)
                <tr>
                    <td>{{$alumno->nombre}}</td>
                    <td>{{$alumno->apellido_paterno}}</td>
                    <td>{{$alumno->apellido_materno}}</td>
                    <td>{{$alumno->email}}</td>
                    <td>{{$alumno->telefono}}</td>
                    <td> <a class="text-primary" href="{{route('alumno.edit', $alumno)}}"><i class="fas fa-edit"></i></a></td>
                    <td><a class="text-danger" href="{{route('alumno.destroy', $alumno)}}"><i class="fas fa-trash-alt"></a></td>
                    <td><a href="{{route('clase.create', $alumno)}}">asignar clase</a></td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>

</div>
{{$alumnos->links()}}

@endsection