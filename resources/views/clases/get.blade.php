@extends('Layout.Principal')
@section('title', 'Alumno' )

@section('content')
<div class="container">

    <h1>clases asignadas</h1>
    <a class="btn btn-success" href="{{route('alumno.index')}}">Alumnos registrados</a>
    <br>
    <br>
    <div class="row">
        <table class="table table-stripper table-success" id="table-alumn">
            <thead>
                <tr>
                    <td>Nombre de la clase</td>
                    <td>Nombre del Alumno</td>
                    <td>turno</td>
                    <td>grupo</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($clases as $clase)
                <tr>
                    <td>{{$clase->nombre_materia}}</td>
                    <td>{{$clase->nombre}} {{$clase->apellido_paterno}} {{$clase->apellido_materno}} </td>
                    <td>{{$clase->turno}}</td>
                    <td>{{$clase->grupo}}</td>
                    <td><a href="{{route('parcial.create', $clase->id)}}">calificar</a></td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>

</div>
{{$clases->links()}}

@endsection