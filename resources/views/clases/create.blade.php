@extends('Layout.Principal')
@section('title', 'Alumno' )

@section('content')
<div class="container">

<h1>Registrar clase</h1>
<br>

<form action="{{route('clase.stoke')}}" method="POST" >
        @csrf 
        <input class="form-control"type="text" name="id_alumno"value="{{$alumno->id}}" hidden>
               <div class="row">
                  <div class="col-2">
                     <label for="nombre">Nombre del alumno</label>
                  </div>
                  <div class="col-4">
                  <input class="form-control"type="text"  placeholder="{{$alumno->nombre . ' '. $alumno->apellido_paterno. ' '. $alumno->apellido_materno}}" disabled="disabled" >
                  </div>
               </div>
               @error('id_alumno')
               <small> {{$message}}</small>    
                @enderror
               <br>
               <div class="row">
               <select class="form-control col-4" name="id_materia">
                  <option>Selecciona la materia</option>
                  @foreach ($materias as $materia)
                         <option value="{{$materia->id}}">{{$materia->nombre_materia}}</option>
                  @endforeach
            </select>
               </div>
               <br>
               <div class="row">
               <select class="form-control col-4" name="turno">
                  <option>seleciona el turno</option>
                  <option value="matutino">maturino</option>
                  <option value="despertino">despertino</option>
            </select>
               </div>
               <br>
               <div class="row">
               <select class="form-control col-4" name="grupo">
                  <option>seleciona el grupo</option>
                  <option value="s1">s1</option>
                  <option value="s2">s2</option>
                  <option value="s3">s3</option>
            </select>
               </div>
              <br>
            <input type="submit" class="btn btn-success" value="Registrar clases">
       </form>
</div>

@endsection

