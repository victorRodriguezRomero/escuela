<?php

namespace App\Http\Controllers;

use App\Models\alumno;
use App\Models\materia;
use App\Models\clase;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CLasesController extends Controller
{
    function getclases()
    {
        $clases= DB::table('clases')->join('alumnos', 'clases.id_alumno', '=', 'alumnos.id')
        ->join('materias', 'clases.id_materia', '=', 'materias.id')
        ->select('clases.*', 'alumnos.nombre', 'alumnos.apellido_paterno', 'alumnos.apellido_materno', 
        'materias.nombre_materia')->simplePaginate(5);
        return view('clases.get', compact('clases'));
    }
    function Create($id)
    {
        $alumno = alumno::find($id);
        $materias = materia::all();
        return view('clases.create')->with(compact('alumno', 'materias'));
    }
    function stoke(Request $request)
    {
        $clase = new clase();
        try {
            $validarClase=DB::table('clases')->find($request->id_alumno);
            
            if( $validarClase->id_materia==$request->id_materia){
                return "materia ya asignada al alumno";
            }
            else{
                $clase->turno = $request->turno;
                $clase->grupo = $request->grupo;
                $clase->id_alumno = $request->id_alumno;
                $clase->id_materia = $request->id_materia;
    
                $clase->save();
                return redirect()->route('clase.index')->with('la asignacion de la clase fue exitosa');
            }
           
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
