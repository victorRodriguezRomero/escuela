<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\clase;
use Illuminate\Support\Facades\DB;
class PartialController extends Controller
{
    public function Create($id){
        $clases= clase::find($id);
        $nombre= DB::table('alumnos')->select("nombre")->where('id', '=', $clases->id)->get();
        $materia= DB::table('materias')->select("nombre_materia")->where('id', '=', $clases->id)->get();

        return $nombre . $materia;
        
    }
}
