<?php

namespace App\Http\Controllers;

use App\Models\alumno;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AlumnController extends Controller
{
    function index()
    {

        $alumnos = alumno::paginate(5);
        return view('Alumno.index', compact('alumnos'));
    }

    function create()
    {
        return view('Alumno.create');
    }
    
    function add(Request $request)
    {
        $alumno = new alumno();
        try {

            $email = $request->email;
            $valided_email = DB::table('alumnos')->select('id')->where('email', '=', $email)->get();
            if (!$valided_email->isEmpty()) {

                return $valided_email . "Correo ya registrado";
            } else {

                $alumno->nombre = $request->nombre;
                $alumno->apellido_paterno = $request->apellido_paterno;
                $alumno->apellido_materno = $request->apellido_materno;
                $alumno->email = $request->email;
                $alumno->telefono = $request->telefono;
                $alumno->save();
                return redirect()->route('alumno.create');
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function edit($id)
    {
        $alumno = alumno::find($id);
        return view('alumno.edit', compact('alumno'));
    }
    public function update(Request $request, $id)
    {

        $alumno = alumno::find($id);
        $alumno->nombre = $request->nombre;
        $alumno->apellido_paterno = $request->apellido_paterno;
        $alumno->apellido_materno = $request->apellido_materno;
        $alumno->email = $request->email;
        $alumno->telefono = $request->telefono;
        $alumno->save();
        return redirect()->route('alumno.index', $alumno);
    }

    public function destroy($id)
    {
        $alumno = alumno::find($id);
        $alumno->delete();
        return redirect()->route('alumno.index', $alumno)->with('notice', 'El usuario ha sido eliminado correctamente.');
    }
}
